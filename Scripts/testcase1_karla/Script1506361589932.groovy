import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('http://localhost:3000/#main_dashboard')

WebUI.setText(findTestObject('test_1karla/Page_Dave Smith Motors/input_txtUser'), 'admin')

WebUI.setText(findTestObject('test_1karla/Page_Dave Smith Motors/input_txtPassword'), 'admin')

WebUI.click(findTestObject('test_1karla/Page_Dave Smith Motors/button_LOGIN'))

WebUI.click(findTestObject('test_1karla/Page_Dave Smith Motors (1)/span_icon'))

WebUI.click(findTestObject('test_1karla/Page_Dave Smith Motors (1)/span_icon'))

WebUI.click(findTestObject('test_1karla/Page_Dave Smith Motors (1)/footer'))

WebUI.click(findTestObject('test_1karla/Page_Dave Smith Motors (1)/footer'))

WebUI.click(findTestObject('test_1karla/Page_Dave Smith Motors (1)/section_load_content'))

WebUI.click(findTestObject('test_1karla/Page_Dave Smith Motors (1)/button_button dropdown'))

WebUI.click(findTestObject('test_1karla/Page_Dave Smith Motors (1)/a_Approval'))

WebUI.setText(findTestObject('test_1karla/Page_Dave Smith Motors (1)/input_tya tt-input'), 'u')

WebUI.sendKeys(findTestObject('test_1karla/Page_Dave Smith Motors (1)/input_tya tt-input'), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('test_1karla/Page_Dave Smith Motors (1)/input_customer_cash_down'), '0')

WebUI.setText(findTestObject('test_1karla/Page_Dave Smith Motors (1)/input_operations'), '12000')

WebUI.setText(findTestObject('test_1karla/Page_Dave Smith Motors (1)/input_operations (1)'), '12')

WebUI.setText(findTestObject('test_1karla/Page_Dave Smith Motors (1)/input_operations (2)'), '7.')

WebUI.click(findTestObject('test_1karla/Page_Dave Smith Motors (1)/div_reveal-modal-bg'))

WebUI.setText(findTestObject('test_1karla/Page_Dave Smith Motors (1)/input_operations (2)'), '7.5')

WebUI.selectOptionByValue(findTestObject('test_1karla/Page_Dave Smith Motors (1)/select'), '1', true)

WebUI.click(findTestObject('test_1karla/Page_Dave Smith Motors (1)/button_Complete'))

WebUI.click(findTestObject('test_1karla/Page_Dave Smith Motors (2)/i_area-user icon-chevron-down'))

WebUI.click(findTestObject('test_1karla/Page_Dave Smith Motors (2)/a_Log Out'))

WebUI.closeBrowser()

