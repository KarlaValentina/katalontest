<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_icon</name>
   <tag></tag>
   <elementGuidId>b3237b0f-feda-4e8d-87c2-381fe6c77728</elementGuidId>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main_dashboard&quot;)/div[@class=&quot;row-dashboard&quot;]/div[@class=&quot;pnl-bms&quot;]/table[@class=&quot;gv-bms&quot;]/tbody[1]/tr[@class=&quot;principal&quot;]/td[@class=&quot;bm-options&quot;]/span[@class=&quot;icon&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>icon</value>
   </webElementProperties>
</WebElementEntity>
